/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package CasoCajeros;


import java.io.File;
import java.util.ArrayList;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerConfigurationException;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;


import org.w3c.dom.Document;
import org.w3c.dom.Element;
/**
 *
 * @author diego
 */
public class CasoCajeros {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) throws ParserConfigurationException, 
            TransformerConfigurationException, TransformerException  {
        // TODO code application logic here
        Cliente c1 =new Cliente("juan","qwe",12,123,123);
        Cliente c2 =new Cliente("Pedro","qwe",12,123,123);
        Cliente c3 =new Cliente("Esteban","qwe",12,123,123);
        Cliente c4 =new Cliente("Sara","qwe",12,123,123);
        Cliente c5 =new Cliente("Daniela","qwe",12,123,123);
        Cliente c6 =new Cliente("Pamela","qwe",12,123,123);
        Cliente c7 =new Cliente("Luis","qwe",12,123,123);
        Cliente c8 =new Cliente("Carlos","qwe",12,123,123);
        Cliente c9 =new Cliente("Camilo","qwe",12,123,123);
        Cliente c10 =new Cliente("Laura","qwe",12,123,123);
        Cliente c11 =new Cliente("Noemi","qwe",12,123,123);
        Cliente c12 =new Cliente("Pablo","qwe",12,123,123);
        Cliente c13 =new Cliente("Camila","qwe",12,123,123);
        Cliente c14 =new Cliente("Loreto","qwe",12,123,123);
        Cliente c15 =new Cliente("Ricardo","qwe",12,123,123);
        Cliente c16 =new Cliente("Antonio","qwe",12,123,123);
        Cliente c17 =new Cliente("Carolina","qwe",12,123,123);
        Cliente c18 =new Cliente("Maria","qwe",12,123,123);
        Cliente c19 =new Cliente("Catalina","qwe",12,123,123);
        Cliente c20 =new Cliente("Denisse","qwe",12,123,123);
        
        c1.addTarjeta(new Tarjeta(12,432,142));
        c1.addTarjeta(new Tarjeta(124,432,142));
        c1.addCuenta(new Cuenta(123,134,"awe",24,c1.getTarjetas().get(0)));
        c1.addCuenta(new Cuenta(123,134,"awe",24,c1.getTarjetas().get(1)));
        
        c2.addTarjeta(new Tarjeta(22344, 23, 834));
        c2.addTarjeta(new Tarjeta(23434, 23, 834));
        c2.addTarjeta(new Tarjeta(2342, 23, 834));
        c2.addTarjeta(new Tarjeta(2345, 23, 834));
        c2.addCuenta(new Cuenta(123,134,"awe",24,c2.getTarjetas().get(0)));
        c2.addCuenta(new Cuenta(123,134,"awe",24,c2.getTarjetas().get(1)));
        c2.addCuenta(new Cuenta(123,134,"awe",24,c2.getTarjetas().get(2)));
        c2.addCuenta(new Cuenta(123,134,"awe",24,c2.getTarjetas().get(3)));
        
        c3.addTarjeta(new Tarjeta(234, 23, 834));
        c3.addCuenta(new Cuenta(82,132,"qwer",23,c3.getTarjetas().get(0)));
        
        c4.addTarjeta(new Tarjeta(234, 23, 834));
        c4.addTarjeta(new Tarjeta(234, 23, 834));
        c4.addCuenta(new Cuenta(82,132,"qwer",23,c4.getTarjetas().get(0)));
        c4.addCuenta(new Cuenta(82,132,"qwer",23,c4.getTarjetas().get(1)));
        
        c5.addTarjeta(new Tarjeta(234, 23, 834));
        c5.addCuenta(new Cuenta(82,132,"qwer",23,c5.getTarjetas().get(0)));
       
        c6.addTarjeta(new Tarjeta(234, 23, 834));
        c6.addCuenta(new Cuenta(82,132,"qwer",23,c6.getTarjetas().get(0)));
        
        c7.addTarjeta(new Tarjeta(234, 23, 834));
        c7.addCuenta(new Cuenta(82,132,"qwer",23,c7.getTarjetas().get(0)));
        
        c8.addTarjeta(new Tarjeta(234, 23, 834));
        c8.addCuenta(new Cuenta(82,132,"qwer",23,c8.getTarjetas().get(0)));
        
        c9.addTarjeta(new Tarjeta(234, 23, 834));
        c9.addCuenta(new Cuenta(82,132,"qwer",23,c9.getTarjetas().get(0)));
        
        c10.addTarjeta(new Tarjeta(234, 23, 834));
        c10.addCuenta(new Cuenta(82,132,"qwer",23,c10.getTarjetas().get(0)));
        
        c11.addTarjeta(new Tarjeta(234, 23, 834));
        c11.addCuenta(new Cuenta(82,132,"qwer",23,c11.getTarjetas().get(0)));
        
        c12.addTarjeta(new Tarjeta(234, 23, 834));
        c12.addCuenta(new Cuenta(82,132,"qwer",23,c12.getTarjetas().get(0)));
        
        c12.addTarjeta(new Tarjeta(234, 23, 834));
        c12.addCuenta(new Cuenta(82,132,"qwer",23,c12.getTarjetas().get(0)));
        
        c13.addTarjeta(new Tarjeta(234, 23, 834));
        c13.addCuenta(new Cuenta(82,132,"qwer",23,c13.getTarjetas().get(0)));
        
        c14.addTarjeta(new Tarjeta(234, 23, 834));
        c14.addCuenta(new Cuenta(82,132,"qwer",23,c14.getTarjetas().get(0)));
        
        c15.addTarjeta(new Tarjeta(234, 23, 834));
        c15.addCuenta(new Cuenta(82,132,"qwer",23,c15.getTarjetas().get(0)));
        
        c16.addTarjeta(new Tarjeta(234, 23, 834));
        c16.addCuenta(new Cuenta(82,132,"qwer",23,c16.getTarjetas().get(0)));
        
        c17.addTarjeta(new Tarjeta(234, 23, 834));
        c17.addCuenta(new Cuenta(82,132,"qwer",23,c17.getTarjetas().get(0)));
        
        c18.addTarjeta(new Tarjeta(234, 23, 834));
        c18.addCuenta(new Cuenta(82,132,"qwer",23,c18.getTarjetas().get(0)));
        
        c19.addTarjeta(new Tarjeta(234, 23, 834));
        c19.addCuenta(new Cuenta(82,132,"qwer",23,c19.getTarjetas().get(0)));
        
        c20.addTarjeta(new Tarjeta(234, 23, 834));
        c10.addCuenta(new Cuenta(82,132,"qwer",23,c20.getTarjetas().get(0)));
      
        ArrayList<Cliente>clientes = new ArrayList();
        clientes.add(c1);
        clientes.add(c2);
        clientes.add(c3);
        clientes.add(c4);
        clientes.add(c5);
        clientes.add(c6);
        clientes.add(c7);
        clientes.add(c8);
        clientes.add(c9);
        clientes.add(c10);
        clientes.add(c11);
        clientes.add(c12);
        clientes.add(c13);
        clientes.add(c14);
        clientes.add(c15);
        clientes.add(c16);
        clientes.add(c17);
        clientes.add(c18);
        clientes.add(c19);
        clientes.add(c20);
        
        
        
        DocumentBuilderFactory docFact= DocumentBuilderFactory.newInstance();
        DocumentBuilder docBuilder = docFact.newDocumentBuilder();
        
        Document doc = docBuilder.newDocument();
        Element rootElement = doc.createElement("Clientes");
        doc.appendChild(rootElement);
        
        for (int i = 0; i < clientes.size(); i++) {
            Element cli = doc.createElement("Cliente");
            rootElement.appendChild(cli);

            Element name= doc.createElement("nombre");
            name.appendChild(doc.createTextNode(clientes.get(i).getNombre()));
            cli.appendChild(name);

            Element direccion= doc.createElement("direccion");
            direccion.appendChild(doc.createTextNode(clientes.get(i).getDireccion()));
            cli.appendChild(direccion);


            Element numCliente= doc.createElement("numCliente");
            numCliente.appendChild(doc.createTextNode(""+clientes.get(i).getNumCliente()));
            cli.appendChild(numCliente);

            Element identificacion= doc.createElement("identificacion");
            identificacion.appendChild(doc.createTextNode(""+clientes.get(i).getIdentificacion()));
            cli.appendChild(identificacion);   

            Element telefono= doc.createElement("telefono");
            telefono.appendChild(doc.createTextNode(""+clientes.get(i).getTelefono()));
            cli.appendChild(telefono); 
            
            
            for (int j = 0; j < clientes.get(i).getCuentas().size(); j++) {
               
                Element cuenta = doc.createElement("cuenta");
                cli.appendChild(cuenta);
                
                Element numCuentaC = doc.createElement("numCuenta");
                numCuentaC.appendChild(doc.createTextNode(""+clientes.get(i).getCuentas().get(j).getNumCuenta()));
                cuenta.appendChild(numCuentaC);
                
                Element numClienteC = doc.createElement("numCliente");
                numClienteC.appendChild(doc.createTextNode(""+clientes.get(i).getCuentas().get(j).getNumCliente()));
                cuenta.appendChild(numClienteC);
                
                Element tipoDeCuenta = doc.createElement("tipoDeCuenta");
                tipoDeCuenta.appendChild(doc.createTextNode(""+clientes.get(i).getCuentas().get(j).getTipoDeCuenta()));
                cuenta.appendChild(tipoDeCuenta);
                
                Element saldo = doc.createElement("saldo");
                saldo.appendChild(doc.createTextNode(""+clientes.get(i).getCuentas().get(j).getSaldo()));
                cuenta.appendChild(saldo);
                
                Element tarjeta= doc.createElement("tarjeta");
                cuenta.appendChild(tarjeta);
                
                Element numCuenta= doc.createElement("numCuenta");
                numCuenta.appendChild(doc.createTextNode(""+clientes.get(i).getCuentas().get(j).getTarjeta().getNumCuenta()));
                tarjeta.appendChild(numCuenta);
                
                Element numTarjeta= doc.createElement("numTarjeta");
                numTarjeta.appendChild(doc.createTextNode(""+clientes.get(i).getCuentas().get(j).getTarjeta().getNumtarjeta()));
                tarjeta.appendChild(numTarjeta);
                
                Element numClienteT= doc.createElement("numClienteT");
                numClienteT.appendChild(doc.createTextNode(""+clientes.get(i).getCuentas().get(j).getTarjeta().getNumCliente()));
                tarjeta.appendChild(numClienteT);
                
               
            }
           
        }
        
        
        
        TransformerFactory transformerFactory = TransformerFactory.newInstance();
        Transformer transformer = transformerFactory.newTransformer();
	DOMSource source = new DOMSource(doc);
	StreamResult result = new StreamResult(new File("C:\\Users\\diego\\Documents\\NetBeansProjects\\CasoCajeros\\src\\CasoCajeros\\Clientes.xml"));
         
        transformer.transform(source, result);

	System.out.println("File saved!");
    }
 
    
} 
