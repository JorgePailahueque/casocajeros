/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package CasoCajeros;

/**
 *
 * @author diego
 */
public class Cajero {
    private String clave;

    public Cajero(String clave) {
        this.clave = clave;
    }

    public void mostrarOpciones(){}
    public void solicitarClave(){}
    public void VerificaBanco(){}
    public void darRespuesta(){}
    
    public String getClave() {
        return clave;
    }

    public void setClave(String clave) {
        this.clave = clave;
    }
    
    
}
